﻿using System;

using NodaTime; // Отсюда LocalTime
using NetTopologySuite.Geometries;  // Отсюда Point и другая геометрия
using NetTopologySuite.Mathematics; // Отсюда векторы

using OSMLSGlobalLibrary.Modules;  // Отсюда OSMLSModule

using ActorModule;

namespace ActorTestModule
{
    public class ActorTestModule : OSMLSModule
    {
        // Координаты центра Волгограда. Сюда будем закидывать акторов
        private double x = 4956764;
        private double y = 6226529;

        // Зададим радиус, в котором будут ходить акторы
        private double radius = 10000;

        //Генератор случайных чисел
        private Random random = new Random();

        // И случайное смещение от центра, которое будем использовать для создания точек интереса
        private double offset { get { return random.NextDouble() * 2 * radius - radius; } }

        // Этот метод будет вызван один раз при запуске, соответственно тут вся инициализация
        protected override void Initialize()
        {
            // Создаем состояние шаблон. Потом каждому человеку зададим свои точки интересов
            SampleState state = new SampleState()
            {
                Hunger = 100,

                // Интервал можно задать через объекты LocalTime
                HomeTime = new TimeInterval(new LocalTime(20, 0), new LocalTime(8, 0)),

                // А можно через часы и минуты (и секунды тоже, если есть такая необходимость)
                JobTime = new TimeInterval(8, 0, 20, 0)
            };

            // Создаём акторов
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"Creating actor {i + 1}");

                // Делаем для каждого точку дома и точку работы в квадрате заданного радиуса от точки спавна
                state.Home = new Point(x + offset, y + offset);
                state.Job = new Point(x + offset, y + offset);

                Console.WriteLine($"Home at {state.Home.X}, {state.Home.Y}; " +
                                  $"Job at {state.Job.X}, {state.Job.Y}");

                // Создаём актора
                Actor actor = new Actor(x, y);

                // Добавляем компонент состояния. Внутри компонент копируется (тем самым методом copy), так что в принципе
                // можно всех акторов одним и тем же состоянием инициализировать
                actor.AddState(state);

                // Добавляем актора в объекты карты
                MapObjects.Add(actor);
            }

            // Получаем список акторов на карте и выводим их количество
            var actors = MapObjects.GetAll<Actor>();
            Console.WriteLine($"Added {actors.Count} actors");

            foreach (var actor in actors)
                Console.WriteLine($"Actor on ({actor.Coordinate.X}, {actor.Coordinate.Y})\n" +
                                  $"\tHome at {actor.GetState<SampleState>().Home.X}, {actor.GetState<SampleState>().Home.Y}\n" +
                                  $"\tJob at {actor.GetState<SampleState>().Job.X}, {actor.GetState<SampleState>().Job.Y}");
        }

        // Этот метод вызывается регулярно, поэтому тут все действия, которые будут повторяться
        public override void Update(long elapsedMilliseconds)
        {
            Console.WriteLine("\nActorTestModule: Update");

            // Снова получаем список акторов
            var actors = MapObjects.GetAll<Actor>();
            Console.WriteLine($"Got {actors.Count} actors\n");

            // Для каждого актёра проверяем условия и назначаем новую активность если нужно
            foreach (var actor in actors)
            {
                // Достаём нужный компонент состояния
                SampleState state = actor.GetState<SampleState>();

                // Дальше идёт куча првоверок
                // Основная их цель - убедиться, что я не перезаписываю одну и ту же активность на каждой итерации
                // Это может быть полезно, если, например, создание активности затрачивает много времени

                // Не уверен, что это правильный способ решать данную проблему, но другого способа я не знаю

                // Есть ли активность
                bool isActivity = actor.Activity != null;

                // Если активность есть, то наша ли это активность
                bool goActivity = isActivity ? actor.Activity is GoActivity : false;

                // Если активность наша, ведёт ли она актора домой или на работу
                bool goHome = goActivity ? (actor.Activity as GoActivity).Destination == state.Home : false;
                bool goWork = goActivity ? (actor.Activity as GoActivity).Destination == state.Job : false;

                Console.WriteLine($"Flags: {isActivity} {goActivity} {goHome} {goWork}");

                if (state.HomeTime.Ongoing) // Если сейчас время, когда надо идти домой
                {
                    // Если активности нету, или активность не наша, или активность наша, но не ведет домой
                    if (!isActivity || !goActivity || !goHome)
                    {
                        // Назначить актору путь до дома
                        actor.Activity = new GoActivity(state.Home);
                        Console.WriteLine("Said actor go home\n");
                    }

                }
                else if (state.JobTime.Ongoing) // Если сейчас вермя идти на работу
                {
                    // Если активности нету, или активность не наша, или активность наша, но не ведет на работу
                    if (!isActivity || !goActivity || !goWork)
                    {
                        // Назначить актору путь до работы
                        actor.Activity = new GoActivity(state.Job);
                        Console.WriteLine("Said actor go work\n");
                    }
                }

                // Здесь я не проверяю приоритет (не знаю, по каким правилам его стоит задавать), 
                // но его следует проверять до задания активности. Например:
                //
                //     int newPrioriy = ...;  // Считаем приоритет своей активности
                //
                //     if (newPriority > actor.Activity.Priority)  // Задаём активность актору, если приоритет выше текущей
                //         actor.Activity = new GoActivity(actor.GetState<State>().Job, newPriority);
            }
        }
    }

    // Активность которая заставляет актора куда-то идти
    public class GoActivity : IActivity
    {
        // Приоритет делаем авто-свойством, со значением по умолчанию
        // Вообще он дожен был быть полем, но интерфейсы не дают объявлять поля, так что...
        // Внимание! Сделайте сеттер привытным, чтобы дпугие не могли поменять приоритет
        public int Priority { get; private set; } = 1;

        // Точка назначения
        public Point Destination { get; }

        public GoActivity(Point destination)
        {
            Destination = destination;
        }

        public GoActivity(Point destination, int priority)
        {
            Destination = destination;
            Priority = priority;
        }

        // Здесь происходит работа с актором
        public bool Update(Actor actor, double deltaTime)
        {
            double speed = 2;  //Скорость в м/с. По хорошему её в State определить

            // Расстояние, которое может пройти актор с заданной скоростью за прошедшее время
            double distance = speed * deltaTime;

            Console.WriteLine($"\nGoActivity: Update\n" +
                $"Delta: {deltaTime} s; Distance: {distance}");

            // Вектор направления до точки назначения
            Vector2D direction = new Vector2D(actor.Coordinate, Destination.Coordinate);

            // Проверка на перешагивание
            if (direction.Length() <= distance)
            {
                // Шагаем в точку, если она ближе, чем расстояние которое можно пройти
                actor.X = Destination.X;
                actor.Y = Destination.Y;
            }
            else
            {
                // Вычисляем новый вектор, с направлением к точке назначения и длинной в distance
                direction = direction.Normalize().Multiply(distance);

                // Смещаемся по вектору
                actor.X += direction.X;
                actor.Y += direction.Y;
            }

            // Если в процессе шагания мы достигли точки назначения
            if (actor.Coordinate == Destination.Coordinate)
                return true;  // Выходим с флагом true - активность завершена

            return false;
        }
    }
}
