# Общее

Это ActorModule для OSMLifeSimulation.

В модуле реализованы классы:

- `Actor` - Действующее лицо симуляции
- `TimeInterval` - Промежуток времени, ограниченный началом и концом

- `IActivity` - интерфейс активности, реализуя который можно создавать различные возможности поведения для акторов
- `IState` - интерфейс компонента состояния. В реализуемые компоненты можно добавить все необходимые данные, которые требуется хранить при акторе 

Подробнее в референсе ниже

Модуль использует NodaTime для представления времени. [Документация к NodaTime](https://nodatime.org/3.0.x/api/NodaTime.html)

[Методичка](https://docs.google.com/document/d/1ku00EoGw6S3cMoC-EM1d2Z-hHY0rq8_XB4Lj6CGS25E/edit#) к основной программе

# Использование

## Сборка

Для сборки необходимо иметь .NET SDK версии 3.1

Напишите мне, если у кого будут проблемы со сборкой. Если не будет проблем тоже напишите :)

### Windows (cmd)

    git clone https://gitlab.com/Capaldi12/osmlsactormodule.git source
    dotnet publish ./source/ActorModule -c release -o ./source/release
	copy /y .\source\release\ActorModule.dll .
	rmdir /s /q .\source
	
### Linux (bash)

	git clone https://gitlab.com/Capaldi12/osmlsactormodule.git source
    dotnet publish ./source/ActorModule -c release -o ./source/release
	cp -f ./source/release/ActorModule.dll .
	rm -rf ./source
	
P.S. Не теряйте точки

## Подключение к проекту

1. Установить `NodaTime` через NuGet
2. Скопировать собранный (см. выше) файл `ActorModule.dll` в папку проекта
3. Добавить `ActorModule.dll` в зависимости проекта
	1. Обозреватель решений
	2. ПКМ по Зависимости
	3. Добавить ссылку на проект
	4. Обзор
	5. Выбрать нужный файл и нажатьт Добавить
	6. Убедиться, что добавленный файл выбран и нажать OK
4. Добавить `using ActorModule;` в файл с кодом

## Подключение к OSMLS

Советую подготовить все dll в одном месте.

- Скачать NodaTime [отсюда](https://storage.googleapis.com/nodatime/releases/NodaTime-3.0.5.zip) и распаковать `NodaTime.dll` (Или скопировать из папки DLL репозитория)
- Скопировать собранный `ActorModule.dll` в ту же папку (Можно так-же взять из папки DLL, но не рекомендуется)
- Собрать свой модуль и поместить в ту же папку
 
Порядок подключения:

`NodaTime.dll` должен быть подключен раньше чем `ActorModule.dll`  
`ActorModule.dll` должен быть подключен раньше модулей, которые от него зависят (sic!)

# ActorTestModule

Это модуль для тестирования `ActorModule` а так же туториал по нему

- Модуль реализует компонент состояния SampleState, который содержит поля, ранее принадлежавшие классу State
- Модуль реализует активность `GoActivity`, которая отвечает за перемещение актора в заданную точку по прямой с постоянной скоростью
- Модуль создаёт 10 акторов, размещая их в центре Волгограда и задаёт им точки интереса - дом и работу - в некотором радиусе от заданного центра. Также модуль назначает им интервалы времени, когда они находятся на работе или дома   
- Во время обновления модуль проверяет, какой интервал времени сейчас идёт и назначает соответствующую активность, если она ещё не назначена.

## Сборка

### Windows (cmd)

    git clone https://gitlab.com/Capaldi12/osmlsactormodule.git source
    dotnet publish ./source/ActorTestModule -c release -o ./source/release
    copy /y .\source\release\ActorTestModule.dll .
    rmdir /s /q .\source

### Linux (bash)

    git clone https://gitlab.com/Capaldi12/osmlsactormodule.git source
    dotnet publish ./source/ActorTestModule -c release -o ./source/release
    cp -f ./source/release/ActorTestModule.dll .
    rm -rf ./source
	
## Использование

При подключении к проекту убедитесь, что `ActorModule` и `NodaTime` тоже подключены

При подклечении к OSMLS, подключать после `ActorModule`

# Референс

Краткая документация по классам модуля

## Классы

### ActorModule

    public class ActorModule : OSMLSModule

Основной класс модуля. Отвечает за вызовы Update у всех акторов в MapObjects

---

#### Методы

    protected override void Initialize() 

Инициализация модуля. В отладочной конфигурации выводит сообщение и создаёт 10 акторов

---

    public override void Update(long elapsedMilliseconds)

Вызывает Update на всех акторах

- `elapsedMilliseconds` - как я понял, это время прошедшее с начала симуляции

---

### Actor

    public class Actor : Point

[Object](https://docs.microsoft.com/dotnet/api/system.object) => [Geometry](http://nettopologysuite.github.io/NetTopologySuite/api/NetTopologySuite.Geometries.Geometry.html) => [Point](http://nettopologysuite.github.io/NetTopologySuite/api/NetTopologySuite.Geometries.Point.html?q=Point) => Actor

Действующее лицо симуляции. Хранит объект состояния и активность, которую обновляет  
Создаваемых акторов помещайте в MapObjects, чтобы модуль мог их найти и обновлять

---

#### Свойства

    public IActivity Activity { get; set; }

Текущая активность актора, null если не задана. См. интерфейс IActivity

---

#### Конструкторы

    public Actor(double x, double y)

Создает актора с заданными координатами (x, y)

- `x` - начальная абсцисса актора
- `y` - начальная ордината актора

---

    public Actor(Coordinate coordinate)
    
Создает актора с заданными [координатами](http://nettopologysuite.github.io/NetTopologySuite/api/NetTopologySuite.Geometries.Coordinate.html)

- `coordinate` - начальная координата актора

---

#### Методы

	public void AddState(IState state)

Добавляет компонент состояния актору

- `State` - компонент состояния

---

	public T GetState<T>() where T : IState

Возвращает компонент состояния указанного типа (`T`). Если таких несколько, возвращается первый.

---

	public List<T> GetStates<T>() where T : IState

Возвращает список всех компонентов состояния заданного типа (`T`). На случай, если компонентов вашего типа может быть больше одного

---

    public override int GetHashCode()
	
Хэш функция

---

    public override bool Equals(object o)
	
Проверяет, равен ли актор объекту `o`

- `o` - Объект для сравнения

---

#### Закрытые члены

	private readonly List<IState> states;

Список компонентов актора. Обращаться через AddState, GetState и GetStates

---

    internal void Update(double deltaTime)
	
Вызывает текущую активность, если есть  
Удаляет активность, если она завершена

- `deltaTime` - Время прошедшее с предыдущего обновления

---

## Структуры

### TimeInterval

    public readonly struct TimeInterval
    
Представляет интервал времени с заданными временем начала и окончания  
Время начала и окончания представляется в виде [LocalTime](https://nodatime.org/3.0.x/api/NodaTime.LocalTime.html) из [NodaTime](https://nodatime.org/3.0.x/api/NodaTime.html)

Start может быть после End, в таком случае интервал становится мультисуточным (IsMultiDay)  
Started и Ended для мультисуточного интервала работают не интуитивно. Они лишь обозначают, был ли уже в сегодняшних сутках момент начала/конца интервала

Ongoing вроде должен правильно работать — возвращать true, если интервал идёт прямо сейчас

---

#### Свойства

    public LocalTime Start { get; }

Время начала интервала

---

    public LocalTime End { get; }

Время окончания интервала

---

    public DateTime TodayStart { get; }

Время начала интервала в текущие сутки

---

    public DateTime TodayEnd { get; }

Время конца интервала в текущие сутки

---

    public bool IsMultiDay { get; }
	
True, если интервал начинается и заканчивается в разные сутки

---

    public bool Started { get; }

True, если сегодня интервал уже начался

---

    public bool Ended { get; }

True, если сегодня интервал уже закончился

---

    public bool Ongoing { get; }

True, если интервал уже начался, но ещё не закончился

---

    public TimeSpan AsTimeSpan { get; }

Длинна интервала в виде TimeSpan

---

#### Конструкторы

    public TimeInterval(LocalTime start, LocalTime end)

Создает интервал на основе времени начала и окончания ([LocalTime](https://nodatime.org/3.0.x/api/NodaTime.LocalTime.html))

- `start` - Время начала интервала
- `end` - Время окончания интервала

---

    public TimeInterval(int startHour, int startMinute, int endHour, int endMinute)

Создает интервал на основе часа и минуты начала и окончания

- `startHour` - Час начала
- `startMinute` - Минута начала
- `endHour` - Час окончания
- `endMinute` - Минута окончания

---

    public TimeInterval(int startHour, int startMinute, int startSecond, int endHour, int endMinute, int endSecond)

Создает интервал на основе часа, минуты и секунды начала и окончания

- `startHour` - Час начала
- `startMinute` - Минута начала
- `startSecond` - Секунда
- `endHour` - Час окончания
- `endMinute` - Минута окончания
- `endSecond` - Секунда окончания

---

#### Закрытые члены

    private static TimeSpan TimeToSpan(LocalTime local)

Превращает LocalTime в TimeSpan для работы с .NET-овскими датами

- `local` - LocalTime для конверсии

## Интерфейсы

### IActivity

    public interface IActivity
    
Активность, которую можно задать актору

---

#### Свойства

    int Priority { get; }

Приоритет активности. Используется для сравнения важности активностей

---

#### Методы

    bool Update(Actor actor, double deltaTime)

Обновление активности. В этом методе необходимо выполнять все действия. Должен возвращать True, если активность завершена и может быть удалена

- `Actor` - актор на котором вызвана активность
- `deltaTime` - время в секундах с прошлого обновления

---

### IState

    public interface IState

Компонент состояния актора. Может содержать любые поля

---

#### Методы

    IState Copy();

Выполняет копирование компонента (глубокое). Необходимо убедиться, чтобы копия никак не была связана с оригиналом, иначе какие-либо изменения в одном из них могут изменить и другой, что совсем не желательно
