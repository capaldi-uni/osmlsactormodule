﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

using OSMLSGlobalLibrary.Map;

using NetTopologySuite.Geometries;

namespace ActorModule
{
    /// <summary>
    /// Действующее лицо симуляции. Хранит объект состояния и активность, которую обновляет<br/>
    /// Создаваемых акторов помещайте в MapObjects, чтобы модуль мог их найти и обновлять
    /// </summary>
    [CustomStyle(
        @"new style.Style({
            image: new style.Circle({
                opacity: 1.0,
                scale: 1.0,
                radius: 5,
                fill: new style.Fill({
                    color: 'rgba(255,127,80,1)'
                }),
                stroke: new style.Stroke({
                    color: 'rgba(0, 0, 0, 0.4)',
                    width: 1
                })
            })
        });"
    )]
    public class Actor : Point
    {
#if DEBUG
        private IActivity _activity = null;
        public IActivity Activity
        {
            get { return _activity; }
            set
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;

                if (_activity != null && value == null)
                    Console.WriteLine($"Removing {_activity.GetType().Name} with priority of {_activity.Priority}");
                else if (_activity == null && value != null)
                    Console.WriteLine($"Setting new {value.GetType().Name} with priority of {value.Priority}");
                else if (_activity != null && value != null)
                    Console.WriteLine($"Replacing {_activity.GetType().Name}({_activity.Priority}) " +
                        $"with {value.GetType().Name}({value.Priority})");

                _activity = value;

                Console.ResetColor();
            }
        }
#else

        /// <summary>
        /// Текущая активность актора, null если не задана.
        /// </summary>
        public IActivity Activity { get; set; } = null;
#endif
        /// <summary>
        /// Список компонентов состояния актора.
        /// Обращаться через AddState, GetState и GetStates
        /// </summary>
        private readonly List<IState> states = new List<IState>();

        /// <summary>
        /// Добавляет компонент состояния актору
        /// </summary>
        /// <param name="state">Компонент состояния</param>
        public void AddState(IState state)
        {
            states.Add(state.Copy());
        }

        /// <summary>
        /// Возвращает компонент состояния указанного типа (`T`).
        /// Если таких несколько, возвращается первый.
        /// </summary>
        /// <typeparam name="T">Тип компонента</typeparam>
        /// <returns>Компонент</returns>
        public T GetState<T>() where T : IState
        {
            return states.OfType<T>().First();
        }

        /// <summary>
        /// Возвращает список всех компонентов состояния заданного типа. 
        /// На случай, если компонентов вашего типа может быть больше одного
        /// </summary>
        /// <typeparam name="T">Тип компонентов</typeparam>
        /// <returns>Список компонентов</returns>
        public List<T> GetStates<T>() where T : IState
        {
            return (List<T>)states.OfType<T>();
        }

        /// <summary>
        /// Создает актора с заданными координатами
        /// </summary>
        /// <param name="x">Координата x</param>
        /// <param name="y">Координата y</param>
        public Actor(double x, double y) : base(x, y) { }

        /// <summary>
        /// Создает актора с заданными координатами
        /// </summary>
        /// <param name="coordinate">Координаты актора</param>
        public Actor(Coordinate coordinate) : base(new Coordinate(coordinate)) { }
        // Копирую координаты, иначе акторы, получившие на вход один и тот же объект координат будут делить его

        /// <summary>
        /// Вызывает текущую активность, если есть<br/>
        /// Удаляет активность, если она завершена
        /// </summary>
        /// <param name="deltaTime">Время с предыдущего обновления, в секундах</param>
        internal void Update(double deltaTime)
        {
            if (Activity != null)
                if (Activity.Update(this, deltaTime))
                    Activity = null;
        }

        // Пока положимся на системную реализацию хэширования и сравнения
        /// <summary>
        /// Хэш функция
        /// </summary>
        /// <returns>Уникальный хэш-код актора</returns>
        public override int GetHashCode()
        {
            return RuntimeHelpers.GetHashCode(this);
        }

        /// <summary>
        /// Проверяет, равен ли актор объекту o
        /// </summary>
        /// <param name="o">Объект для сравнения</param>
        /// <returns>Признак равенства</returns>
        public override bool Equals(object o)
        {
            return RuntimeHelpers.Equals(this, o);
        }

#if DEBUG
        internal void vibe() {
            Console.WriteLine($"\n\tVibecheck of actor on ({Coordinate.X}, {Coordinate.Y})");
            if (Activity != null)
                Console.WriteLine($"\tActivity is {Activity.GetType().Name} with priority of {Activity.Priority}");
            else
                Console.WriteLine("\tNo current activity");

            foreach (var state in states)
                if (state.GetType().Name == "SpecState")
                    Console.WriteLine($"\tHealth: {GetField(state, "Health"):0.##}; " +
                                      $"Satiety: {GetField(state, "Satiety"):0.##}; " +
                                      $"Mood: {GetField(state, "Mood"):0.##}; " +
                                      $"Stamina: {GetField(state, "Stamina"):0.##}; " +
                                      $"Money: {GetField(state, "Money"):0.##}; " +
                                      $"Speed: {GetField(state, "Speed"):0.##};");
                else if (state.GetType().Name == "JobState")
                {
                    List<TimeInterval> times = GetField(state, "JobTimes") as List<TimeInterval>;

                    bool isJobTime = false;

                    foreach (TimeInterval interval in times)
                    {
                        isJobTime = isJobTime || interval.Ongoing;
                    }

                    if (isJobTime)
                        Console.WriteLine("\tIt's job time");
                    else
                        Console.WriteLine("\tIt's party time!");
                }
        }

        internal static object GetField(object obj, string name)
        {
            return obj.GetType().GetField(name).GetValue(obj);
        }
#endif
    }
}
