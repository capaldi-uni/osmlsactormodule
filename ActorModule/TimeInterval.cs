﻿using System;

using NodaTime;

namespace ActorModule
{
    /// <summary>
    /// Представляет интервал времени с заданными временем начала и конца<br/>
    /// Время начала и конца представляется в виде LocalTime из NodaTime
    /// 
    /// Start может быть после End, в таком случае интервал становится мультисуточным (IsMultiDay)<br/>
    /// Started и Ended для мультисуточного интервала работают не интуитивно.
    /// Они лишь обозначают, был ли уже в сегодняшних сутках момент начала/конца интервала
    /// 
    /// Ongoing вроде должен правильно работать - возвращать true, если интервал идёт прямо сейчас
    /// </summary>
    public readonly struct TimeInterval
    {
        /// <summary>
        /// Время начала интервала
        /// </summary>
        public LocalTime Start { get; }

        /// <summary>
        /// Время конца интервала
        /// </summary>
        public LocalTime End { get; }

        /// <summary>
        /// Время начала интервала в текущие сутки
        /// </summary>
        public DateTime TodayStart
        {
            get { return DateTime.Today + TimeToSpan(Start); }
        }

        /// <summary>
        /// Время конца интервала в текущие сутки
        /// </summary>
        public DateTime TodayEnd 
        { 
            get { return DateTime.Today + TimeToSpan(End); } 
        }

        /// <summary>
        /// True, если интервал начинается и заканчивается в разные сутки
        /// </summary>
        public bool IsMultiDay
        {
            get { return Start > End; }
        }

        /// <summary>
        /// True, если сегодня интервал уже начался
        /// </summary>
        public bool Started 
        { 
            get { return DateTime.Now >= TodayStart; } 
        }

        /// <summary>
        /// True, если сегодня интервал уже закончился
        /// </summary>
        public bool Ended 
        { 
            get { return DateTime.Now > TodayEnd; } 
        }

        /// <summary>
        /// True, если интервал уже начался, но ещё не закончился
        /// </summary>
        public bool Ongoing 
        { 
            get 
            {
                if (IsMultiDay)
                    return Started || !Ended;

                else
                    return Started && !Ended; 
            } 
        }

        /// <summary>
        /// Длинна интервала в виде TimeSpan
        /// </summary>
        public TimeSpan AsTimeSpan { 
            get 
            {
                Period p = End - Start;
                return new TimeSpan((int)p.Hours, (int)p.Minutes, (int)p.Seconds);
            } 
        }

        /// <summary>
        /// Создает интервал на основе времени начала и конца
        /// </summary>
        /// <param name="start">Время начала</param>
        /// <param name="end">Время конца</param>
        public TimeInterval(LocalTime start, LocalTime end)
        {
            Start = start;
            End = end;
        }

        /// <summary>
        /// Создает интервал на основе часа и минуты начала и конца
        /// </summary>
        /// <param name="startHour">Час начала</param>
        /// <param name="startMinute">Минута начала</param>
        /// <param name="endHour">Час конца</param>
        /// <param name="endMinute">Минута конца</param>
        public TimeInterval(int startHour, int startMinute, int endHour, int endMinute)
        {
            Start = new LocalTime(startHour, startMinute);
            End = new LocalTime(endHour, endMinute);
        }

        /// <summary>
        /// Создает интервал на основе часа, минуты и секунды начала и конца
        /// </summary>
        /// <param name="startHour">Час начала</param>
        /// <param name="startMinute">Минута начала</param>
        /// <param name="startSecond">Секунда начала</param>
        /// <param name="endHour">Час конца</param>
        /// <param name="endMinute">Минута конца</param>
        /// <param name="endSecond">Секунда конца</param>
        public TimeInterval(int startHour, int startMinute, int startSecond, 
                            int endHour, int endMinute, int endSecond)
        {
            Start = new LocalTime(startHour, startMinute, startSecond);
            End = new LocalTime(endHour, endMinute, endSecond);
        }

        /// <summary>
        /// Превращает LocalTime в TimeSpan для работы с .NET-овскими датами
        /// </summary>
        /// <param name="local">Нужный LocalTime</param>
        /// <returns>Заданный LocalTime в виде TimeSpan-а</returns>
        private static TimeSpan TimeToSpan(LocalTime local)
        {
            local.Deconstruct(out int h, out int m, out int s);
            return new TimeSpan(h, m, s);
        }

    }

}
